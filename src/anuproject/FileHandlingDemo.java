/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anuproject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;

/**
 *
 * @author ICIT
 */
public class FileHandlingDemo {
 
    public static void main(String[] args) throws Exception {
        File file = new File("test.txt");
        
//        FileOutputStream fos = new FileOutputStream(file);
//        
//        DataOutputStream dos = new DataOutputStream(fos);
//        
//        String msg = "Line number 2 Hello File Handling";
//        
//        dos.writeUTF(msg);
//        
//        fos.close();
        
        System.out.println("Write Successfully...");

        FileInputStream in = new FileInputStream(file);
        
        DataInputStream dis = new DataInputStream(in);
        
        String file_content = dis.readUTF();
        
        System.out.println(file_content);
        
        dis.close();
        
        in.close();
        
        
        
    }
    
}
