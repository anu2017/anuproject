/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anuproject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

class Student implements Serializable {
    int roll_no;
    String name;

    public Student(int roll_no, String name) {
        this.roll_no = roll_no;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Student{" + "roll_no=" + roll_no + ", name=" + name + '}';
    }
    
}

public class SerializationDemo {

    public static void main(String[] args) throws Exception {
        File file = new File("object_file.txt");

        FileOutputStream out = new FileOutputStream(file);

        ObjectOutputStream oos = new ObjectOutputStream(out);
        
//        List fruits = new ArrayList();
//        
//        fruits.add("Banana");
//        fruits.add("Orange");
        
//        oos.writeObject(fruits);

        Student s1 = new Student(1, "Anu");
        
        oos.writeObject(s1);
        
        oos.close();
        
        out.close();
        
        System.out.println("Write successfully...");

//            FileInputStream in = new FileInputStream(file);
//            
//            ObjectInputStream ois = new ObjectInputStream(in);
//            
//            List object_file_content = (List)ois.readObject();
//            
//            System.out.println(object_file_content);
//            
//            System.out.println(object_file_content.get(1));
    }

}
