package anuproject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ArrayDemo {
    public static void main(String[] args) {
        int sub[] = new int[5];
        
        sub[0] = 1;
        sub[1] = 2;
        sub[2] = 3;
        sub[3] = 4;
        sub[4] = 5;
        
        //int sub[5] = {1,2,3,4,5}
        
        for (int i = 0; i < 5; i++) {
            System.out.println(sub[i]);
        }
        
        int mul_d[][] = new int[2][3];
        
        mul_d[0][0] = 1;
        mul_d[0][1] = 2;
        mul_d[0][2] = 3;
        
        mul_d[1][0] = 4;
        mul_d[1][1] = 5;
        mul_d[1][2] = 6;
        
        List<String> fruits = new ArrayList<>(); //<Generics>
        
        fruits.add("Apple");
        fruits.add("Banana");
        fruits.add("Orange");
        fruits.add("Orange");
//        fruits.add(123);
//        fruits.add(3.14);
        
        System.out.println(fruits.get(1));
        
        System.out.println(fruits);
        
        fruits.remove(2);
        
        System.out.println(fruits);
        
        Set<String> months = new HashSet<>();
        
        months.add("January");
        months.add("Feb");
        months.add("Feb");
        
        System.out.println(months);
        
        
    }
}
