package anuproject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.Properties;

public class PropertiesFileHandlingDemo {
    
    public static void main(String[] args) throws Exception {
        Properties p = new Properties();
    
//        p.setProperty("name","Sonoo Jaiswal");  
//        p.setProperty("email","sonoojaiswal@javatpoint.com");  
//        
//        FileOutputStream out = new FileOutputStream(new File("credentials.properties"));
//        
//        OutputStream os = new ObjectOutputStream(out);
//        
//        p.store(os, "Comment message goes here");
//                
//        os.close();
//        
//        out.close();

        FileInputStream in = new FileInputStream(new File("credentials.properties"));
        
        ObjectInputStream ois = new ObjectInputStream(in);
        
        p.load(in);
        
        System.out.println(p.getProperty("name"));
        
        ois.close();
        in.close();
    }
    
    
}
