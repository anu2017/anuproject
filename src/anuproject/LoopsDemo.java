/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anuproject;

import java.util.Scanner;

/**
 *
 * @author ICIT
 */
public class LoopsDemo {
    public static void main(String[] args) {
        
        
        int num = 7;
        
//        int i = 1;
//        while(i <= 10) { // use when you don't know limit
//            System.out.println(num + " * " + i + " = " + i * num);
//            i++;
//        }
//
//        for(int i = 1; i <= 10; i++) {
//            System.out.println(num + " * " + i + " = " + i * num);
//        }

        String fruits[] = {"Orange", "Grapes", "Banana"};
        
//        for(int i=0; i < fruits.length; i++) {
//            System.out.println(fruits[i]);
//        }
        
//        for(String fruit : fruits) {
//            System.out.println(fruit);
//        }
        Scanner sc = new Scanner(System.in);
        
        do {
            System.out.println("Enter number to find square:");
            num = sc.nextInt();    
            System.out.println(num * num);
        } while (num != 0);
        
    }
}
